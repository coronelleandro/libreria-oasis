﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;

namespace LibreriaOasis.Controllers
{
    public class InicioController : Controller
    {

        //ldc_18
        // GET: Inicio
        public ActionResult Index()
        {
            libreria_oasisEntities context = new libreria_oasisEntities();

            if (null != Session["id"])
            {
                int id = (int)Session["id"];
                user_admin admin = context.user_admin.Where(u=>u.id_admin == id).FirstOrDefault();
                ViewBag.admin = admin;
            }
            product pro = new product();
            List<product> products = context.product.ToList();
            ViewBag.products = products;
            
            return View();
        }

        [HttpPost]
        public ActionResult createProduct(string name, string description_product, double precio, int stock)
        {
            libreria_oasisEntities context = new libreria_oasisEntities();
            product pro = new product();
            pro.name = name;
            pro.stock = stock;
            pro.description_product = description_product;
            pro.precio = precio;
            context.product.Add(pro);
            context.SaveChanges();
            return Redirect("~/Inicio/index");
        }

        [HttpGet]
        public ActionResult deleteProduct(int id)
        {
            libreria_oasisEntities context = new libreria_oasisEntities();
            product product = context.product.Find(id);
            context.product.Remove(product);
            context.SaveChanges();
            return Redirect("~/Inicio/index");
        }

        [HttpGet]
        public ActionResult editProduct(int id)
        {
            libreria_oasisEntities context = new libreria_oasisEntities();
            product product = context.product.Find(id);
            ViewBag.product = product;
            return View();
        }

        [HttpPost]
        public ActionResult editTheProduct(string name, string description_product, double precio, int stock, int id)
        {
            libreria_oasisEntities context = new libreria_oasisEntities();
            product pro = context.product.Find(id);
            pro.name = name;
            pro.stock = stock;
            pro.description_product = description_product;
            pro.precio = precio;
            context.product.Attach(pro);
            context.Entry(pro).State = EntityState.Modified;
            context.SaveChanges();
            return Redirect("~/Inicio/index");
        }

        [HttpPost]
        public ActionResult createAdmin(string name,string mail,string pass)
        {
            libreria_oasisEntities context = new libreria_oasisEntities();
            user_admin admin = new user_admin();
  
            if (pass != "ldc_18") {
                return Redirect("~/Inicio/index");
            }
            admin.pass = pass;
            admin.name = name;
            admin.mail = mail;
            context.user_admin.Add(admin);
            context.SaveChanges();
            return Redirect("~/Inicio/index");
        }

        [HttpPost]
        public ActionResult loginAdmin(string mail, string pass)
        {
            libreria_oasisEntities context = new libreria_oasisEntities();
            user_admin admin = context.user_admin
                               .Where(u => u.mail == mail)
                               .Where(u => u.pass == pass)
                               .FirstOrDefault();

            if (admin == null)
            {
                return Redirect("~/Inicio/index");
            }
            Session["id"] = admin.id_admin;
            context.user_admin.Add(admin);
            context.SaveChanges();
            return Redirect("~/Inicio/index");
        }

        public ActionResult logoutAdmin()
        {
            Session.Clear();
            return Redirect("~/Inicio/index");
        }
    }
}